
# Node JS offline using nedb (Firebase sync)




## Super simple to use

We have following functionalities in this package.

- Insert
- update
- delete
- Search or find


## Firebase initialize

```js
var firebaseinit = offline_nedb.fbinit(apiKey,authDomain,databaseURL,projectId);
```
## To check firebase already initialized or not

```js
var check  = offline_nedb.fbinitCheck();
```

## Insert a record
```js
var test_offline = {
    "test1": '1234',
    "test2": '4321',
  }
  offline_nedb.insertFb("test_offline",test_offline).then(function(result){
    console.log(result);
  })
  ```

## Delete a record
  ```js
  offline_nedb.deleteRecord("test_offline",{"_id":"3vD3TJx4VFZfbqQo"}).then(function(deleted){
    console.log(deleted);
  })
  ```
## Update a record

```js
   offline_nedb.update("test_offline",{"_id":'MoHIq1ZBURbnhgnM'},{"test":"Test"}).then(function(data){
     console.log(data);
   })
   ```
## Search a record or fetch records

fetch has three arguments 
 - path -> database path
 - key -> blank will fetch all records. if key is provided then value will be mandatory.
 - value -> it will search for the value in the db

```js
offline_nedb.fetch("test_offline",'').then(function(result){
    console.log(result);
  })
  ```
