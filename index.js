'use strict';
const firebase = require("firebase");
var Datastore = require('nedb');
const request = require('request');
/**
 * This function is used for initialize the firebase (mandatory)
 * apiKey -> Firebase Api Key
 * authDomain -> firebase
 * databaseURL -> firebase database url
 * projectId -> firebase project id
 * storageBucket -> storageBucket (optional)
 */
module.exports.fbinit = function initialize(apiKey,authDomain,databaseURL,projectId) {
  if(!apiKey){
     throw new TypeError('Firebase api key should not be blank');
  }
  if(!authDomain){
     throw new TypeError('Firebase authDomain should not be blank');
  }
  if(!databaseURL){
     throw new TypeError('Firebase databaseURL should not be blank');
  }
  if(!projectId){
     throw new TypeError('Firebase projectId should not be blank');
  }
  const config = {
    apiKey: apiKey,
    authDomain: authDomain,
    databaseURL: databaseURL,
    projectId:projectId,
  };
  if(!firebase.apps.length){
  var app = firebase.initializeApp(config);
  return {status:1,"message":"firebase successfully initialized"};
}else{
  return {status:0,"message":"firebase already initialized"};
}
};
/**
 * For checking firebase is already initialized or not
 */
module.exports.fbinitCheck = function check() {
  if(!firebase.apps.length){
    return {status:0,"message":"firebase not initialized"};
  }else{
    return {status:0,"message":"firebase already initialized"};
  }
}
/**
 * path -> in which path data will enter to the database
 * details -> Object
 * key -> optional if not provided then random firebase key will generate and insert to the firebase.
          If provided then key will be used for inserting the data.
 */
module.exports.insertFb = function insert(path,details) {
  var offlinedb = new Datastore({ filename: "./offline/"+path+".db", autoload: true });
  return new Promise(function(resolve, reject) {
    var db;
    if(!firebase.apps.length){
       reject('Firebase not initialized');
    }else{
      db = firebase.database();
    }
    details['sync'] = 0;
    offlinedb.insert(details,function(err,inserted){
request('http://www.orbonix.com', function (error, response, body) {
  if(!error){
    details['sync'] = 1;
    db.ref(path+"/"+inserted._id).update(details);
    offlinedb.update({_id:inserted._id},{$set: {"sync":1} },{},function(err,updated){
      console.log(updated);
      if(updated){
          offlinedb.find({_id:inserted._id},function(err,docs){
              resolve({status : 1,"message":'successfully synced',"data":docs});
          });
      }else{
        resolve({status : 1,"message":'Unable to insert record please try again...',"data":inserted});
      }
    });
  }else{
    resolve({status : 1,"message":'successfully inserted',"data":inserted});
  }
});
    })
});
};
/**
 * path-> nedb path to fetch details.
 * options : (optional)
    -> key : key to search for
    -> value : value to compare (can be object for child)
 */
module.exports.fetch = function insert(path,key,value='') {
  return new Promise(function(resolve, reject) {
    var findObj = {};
    findObj[key] = value;
  var offlinedb = new Datastore({ filename: "./offline/"+path+".db", autoload: true });
  if(key.length >0){
      offlinedb.find(findObj,function(err,docs){
        resolve(docs);
      })
  }else{
    offlinedb.find({},function(err,docs){
      resolve(docs);
    })
  }
});
};

/**
 * path -> db path
 * query -> query to find out the data
 * replaceObj -> replace value (object)
 */
module.exports.update = function update(path,query,replaceObj) {
  return new Promise(function(resolve, reject) {
    if(Object.keys(query).length>0 && Object.keys(replaceObj).length>0){
      replaceObj['sync']=0;
      var offlinedb = new Datastore({ filename: "./offline/"+path+".db", autoload: true });
      offlinedb.update(query,{$set:replaceObj},function(err,numReplaced){
          if(err){
            reject(err)
          }else{
            syncFirebase(path);
            resolve(numReplaced+" record updated");
          }
      })
    }

});
};
/**
 * path -> db path
 * query -> query to find out the data
 * multiple -> delete multiple if found by default false
 */
module.exports.deleteRecord = function deleteRecord(path,query,multiple=false) {
  return new Promise(function(resolve, reject) {
    if(Object.keys(query).length>0){
      console.log(query);
      var offlinedb = new Datastore({ filename: "./offline/"+path+".db", autoload: true });
      var removeDb = new Datastore({ filename: "./deletedRecord/"+path+".db", autoload: true });
      offlinedb.find(query,function(err,docs){
        if(!docs.length){
          reject("No record found");
        }else{
          for(let doc in docs){
            if(!docs[doc].sync){
              offlinedb.remove(query,{multi:multiple},function(err,numRemoved){
                resolve(numRemoved+" record deleted");
              })
            }
            if(docs[doc].sync){
              removeDb.insert(docs[doc],function(err,numUpdated){
                console.log(numUpdated);
                offlinedb.remove(query,{multi:multiple},function(err,numRemoved){
                  deleteRecordFirebase(path,docs[doc].id);
                  resolve(numRemoved+" record deleted");
                })
              })
            }
          }
        }
      })
    }else{
      reject("Invalid request");
    }
});
};
function syncFirebase(path){
  var db;
  if(firebase.apps.length){
    db = firebase.database();
  request('http://www.orbonix.com', function (error, response, body) {
    if(!error){
      var offlinedb = new Datastore({ filename: "./offline/"+path+".db", autoload: true });
      offlinedb.find({"sync":0},function(err,docs){
        for(let doc in docs){
          docs[doc].sync = 1;
          db.ref(path+"/"+docs[doc]._id).update(docs[doc]);
          offlinedb.update({"_id":docs[doc]._id},{$set:{"sync":'1'}},function(err,numReplaced){
          });
        }
      })
    }
  });
}
}
function deleteRecordFirebase(path,id){
  var db;
  if(firebase.apps.length){
    db = firebase.database();
  request('http://www.orbonix.com', function (error, response, body) {
    if(!error){
      var offlinedb = new Datastore({ filename: "./deletedRecord/"+path+".db", autoload: true });
      offlinedb.find({},function(err,docs){
        if(docs.length > 0){
          for(let doc in docs){
            db.ref(path+"/"+docs[doc]._id).remove();
            offlinedb.remove({"_id":docs[doc]._id},function(err,removed){
            });
          }
        }
      })

    }
  });
}
}
